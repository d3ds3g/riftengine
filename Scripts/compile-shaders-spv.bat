@echo off
echo [Rift] Compiling shaders to Spir-V

glslc.exe ../Rift.Engine/res/Shaders/Shader.vert -o vert.spv
glslc.exe ../Rift.Engine/res/Shaders/Shader.frag -o frag.spv

move vert.spv ../Rift.Engine/res/Shaders
move frag.spv ../Rift.Engine/res/Shaders

echo [Rift] Compilation exited