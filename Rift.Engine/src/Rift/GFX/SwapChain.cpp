#include "Rift/GFX/SwapChain.h"
#include "Rift/GFX/RendererAPI.h"

#include "Platform/Vulkan/VulkanSwapChain.h"

namespace Rift
{
	SwapChain* SwapChain::_pSingletonInstance = nullptr;

	Ref<SwapChain> SwapChain::Create(void* pWinHandle)
	{
		Ref<SwapChain> result;
		switch (RendererAPI::Current())
		{
		[[likely]] case RendererAPI::API::Vulkan:
			result = CreateRef<VulkanSwapChain>(pWinHandle);
		default:
			break;
		}
		_pSingletonInstance = result.get();
		return result;
	}
}

