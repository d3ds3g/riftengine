#ifndef RIFT_SHADER_H
#define RIFT_SHADER_H

#include "Rift/Core/Base.h"

#include <fstream>

namespace Rift
{
	struct RIFT_API ShaderInfo
	{
		union
		{
			char* VertexShaderFile;
			char* VertexShaderSource;
		};

		union
		{
			char* FragmentShaderFile;
			char* FragmentShaderSource;
		};

		bool FileMode = true;
		bool Compiled = false;
	};

	class RIFT_API RIFT_DECLSPEC Shader
	{
	public:
		virtual ~Shader() noexcept = default;

		static Ref<Shader> Create(const ShaderInfo& info);
	};
}

#endif