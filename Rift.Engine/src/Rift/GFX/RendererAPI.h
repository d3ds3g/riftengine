#ifndef RIFT_RENDERER_API_H
#define RIFT_RENDERER_API_H

#include "Rift/Core/Base.h"

namespace Rift
{
	class RIFT_API RIFT_DECLSPEC RendererAPI
	{
	public:
		enum class API : uint8_t
		{
			None = 0,
			OpenGL = 1,
			Vulkan = 2,
			DirectX = 3
		};

		virtual ~RendererAPI() noexcept = default;

		virtual void Init() = 0;

		static Ref<RendererAPI> Create();
		static API Current() { return _API; }
	private:
		static API _API;
	};
}

#endif // !RIFT_RENDERER_API_H
