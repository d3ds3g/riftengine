#ifndef RIFT_GRAPHICS_CONTEXT_H
#define RIFT_GRAPHICS_CONTEXT_H

#include "Rift/Core/Base.h"
#include "Rift/GFX/ValidationLayer.h"

namespace Rift
{
	class RIFT_API RIFT_DECLSPEC GraphicsContext
	{
	protected:
		static GraphicsContext* _pSingletonInstance;
	public:
		virtual ~GraphicsContext() noexcept = default;

		virtual void Init(void* pWinHandle) = 0;
		virtual uint32_t ExtensionCount() = 0;
		virtual void* RawContext() noexcept = 0;

		static Ref<GraphicsContext> Create();
		static GraphicsContext* Get() noexcept { return _pSingletonInstance; }
	};
}

#endif // !RIFT_RENDERER_API_H
