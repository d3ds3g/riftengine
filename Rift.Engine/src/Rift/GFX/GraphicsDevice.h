#ifndef RIFT_GFXDEVICE_H
#define RIFT_GFXDEVICE_H

#include "Rift/Core/Base.h"

#include <optional>

namespace Rift
{
	struct RIFT_API QueueFamiliesIndices
	{
		std::optional<uint32_t> GraphicsFamily;
		std::optional<uint32_t> PresentFamily;

		explicit operator bool()
		{
			return GraphicsFamily && PresentFamily;
		}
	};

	class RIFT_API RIFT_DECLSPEC GraphicsPhysicalDevice
	{
	protected:
		static GraphicsPhysicalDevice* _pSingletonInstance;
	public:
		virtual ~GraphicsPhysicalDevice() noexcept = default;

		virtual void* GetRawDevice() = 0;
		virtual QueueFamiliesIndices& QueuesSupported() noexcept = 0;

		static GraphicsPhysicalDevice* Get() noexcept { return _pSingletonInstance; }
		static Ref<GraphicsPhysicalDevice> Create();
	};

	class RIFT_API RIFT_DECLSPEC GraphicsLogicalDevice
	{
	protected:
		static GraphicsLogicalDevice* _pSingletonInstance;
	public:
		virtual ~GraphicsLogicalDevice() noexcept = default;

		virtual void* GetRawDevice() = 0;

		static GraphicsLogicalDevice* Get() noexcept { return _pSingletonInstance; }
		static Ref<GraphicsLogicalDevice> Create();
	};
}

#endif