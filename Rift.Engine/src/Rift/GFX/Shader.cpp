#include "Rift/GFX/Shader.h"
#include "Rift/GFX/RendererAPI.h"
#include "Platform/Vulkan/VulkanShader.h"

namespace Rift
{
	Ref<Shader> Shader::Create(const ShaderInfo& info)
	{
		switch (RendererAPI::Current())
		{
		[[likely]] case RendererAPI::API::Vulkan:
			return CreateRef<VulkanShader>(info);
		default:
			break;
		}
	}

}
