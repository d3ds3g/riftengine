#include "Rift/GFX/GraphicsDevice.h"
#include "Rift/GFX/RendererAPI.h"

#include "Platform/Vulkan/VulkanGraphicsDevice.h"

namespace Rift
{
	GraphicsPhysicalDevice* GraphicsPhysicalDevice::_pSingletonInstance = nullptr;

	GraphicsLogicalDevice* GraphicsLogicalDevice::_pSingletonInstance = nullptr;

	Ref<GraphicsPhysicalDevice> GraphicsPhysicalDevice::Create()
	{
		Ref<GraphicsPhysicalDevice> result;
		switch (RendererAPI::Current())
		{
		[[likely]] case RendererAPI::API::Vulkan:
			result = CreateRef<VulkanGraphicsPhysicalDevice>();
		default:
			break;
		}
		_pSingletonInstance = result.get();
		return result;
	}

	Ref<GraphicsLogicalDevice> GraphicsLogicalDevice::Create()
	{
		Ref<GraphicsLogicalDevice> result;
		switch (RendererAPI::Current())
		{
		[[likely]] case RendererAPI::API::Vulkan:
			result = CreateRef<VulkanGraphicsLogicalDevice>();
		default:
			break;
		}
		_pSingletonInstance = result.get();
		return result;
	}

}
