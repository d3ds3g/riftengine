#include "Rift/GFX/GraphicsSurface.h"
#include "Rift/GFX/RendererAPI.h"
#include "Platform/Vulkan/VulkanGraphicsSurface.h"

namespace Rift
{
	GraphicsSurface* GraphicsSurface::_pSingletonInstance;

	Ref<GraphicsSurface> GraphicsSurface::Create(void* pWinHandle)
	{
		Ref<GraphicsSurface> result;
		switch (RendererAPI::Current())
		{
		[[likely]] case RendererAPI::API::Vulkan:
			result = CreateRef<VulkanGraphicsSurface>(pWinHandle);
		default:
			break;
		}
		_pSingletonInstance = result.get();
		return result;
	}
}