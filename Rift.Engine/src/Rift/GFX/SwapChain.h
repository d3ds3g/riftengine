#ifndef RIFT_SWAP_CHAIN_H
#define RIFT_SWAP_CHAIN_H

#include "Rift/Core/Base.h"

namespace Rift
{
	class RIFT_API RIFT_DECLSPEC SwapChain
	{
	private:
		static SwapChain* _pSingletonInstance;
	public:
		virtual ~SwapChain() noexcept = default;

		virtual std::pair<uint32_t, uint32_t> Size() = 0;

		static SwapChain* Get() { return _pSingletonInstance; }
		static Ref<SwapChain> Create(void* pWinHandle);
	};
}

#endif // !RIFT_SWAP_CHAIN_H
