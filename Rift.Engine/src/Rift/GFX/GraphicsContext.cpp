#include "Rift/GFX/GraphicsContext.h"
#include "Rift/GFX/RendererAPI.h"

#include "Platform/Vulkan/VulkanGraphicsContext.h"

namespace Rift
{
	GraphicsContext* GraphicsContext::_pSingletonInstance = nullptr;

	Ref<GraphicsContext> GraphicsContext::Create()
	{
		Ref<GraphicsContext> result; 
		switch (RendererAPI::Current())
		{
		[[likely]] case RendererAPI::API::Vulkan:
			result = CreateRef<VulkanGraphicsContext>();
		default:
			break;
		}
		_pSingletonInstance = result.get();
		return result;
	}

}
