#include "Rift/GFX/ValidationLayer.h"
#include "Rift/GFX/RendererAPI.h"

#include "Platform/Vulkan/VulkanValidationLayer.h"

namespace Rift
{
	Ref<ValidationLayer> ValidationLayer::_Instance;

	void ValidationLayer::Init()
	{
		switch (RendererAPI::Current())
		{
		[[likely]] case RendererAPI::API::Vulkan:
			_Instance =  CreateRef<VulkanValidationLayer>();
		default:
			break;
		}
	}

	void ValidationLayer::Attach(const char* pLayerName)
	{
		_Instance->ImplAttach(pLayerName);
	}

	uint32_t ValidationLayer::EnabledLayersCount()
	{
		return _Instance->ImplEnabledLayersCount();
	}

	const char** ValidationLayer::EnabledLayers()
	{
		return _Instance->ImplEnabledLayers();
	}

}

