#ifndef RIFT_GRAPHICS_SURFACE_H
#define RIFT_GRAPHICS_SURFACE_H

#include "Rift/Core/Base.h"

namespace Rift
{
	class RIFT_API RIFT_DECLSPEC GraphicsSurface
	{
	protected:
		static GraphicsSurface* _pSingletonInstance;
	public:
		virtual ~GraphicsSurface() noexcept = default;

		virtual void* RawSurface() = 0;

		static GraphicsSurface* Get() noexcept { return _pSingletonInstance; }
		static Ref<GraphicsSurface> Create(void* pWinHandle);
	};
}

#endif // !RIFT_GRAPHICS_SURFACE_H
