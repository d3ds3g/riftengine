#include "Rift/GFX/RendererAPI.h"

namespace Rift
{
	RendererAPI::API RendererAPI::_API = RendererAPI::API::Vulkan;

	Ref<RendererAPI> RendererAPI::Create()
	{
		switch (_API)
		{
		case API::Vulkan:
			break;
		case API::OpenGL:
			break;
		}
		return {};
	}
}