#ifndef RIFT_VALIDATION_LAYER_H
#define RIFT_VALIDATION_LAYER_H

#include "Rift/Core/Base.h"

namespace Rift
{
	class RIFT_API RIFT_DECLSPEC ValidationLayer
	{
	private:
		static Ref<ValidationLayer> _Instance;
	protected:
		virtual ~ValidationLayer() noexcept = default;
		
		virtual void ImplAttach(const char* pLayerName) = 0;

		virtual uint32_t ImplEnabledLayersCount() = 0;
		virtual const char** ImplEnabledLayers() = 0;

	public:
		static void Init();

		static void Attach(const char* pLayerName);
		static uint32_t EnabledLayersCount();
		static const char** EnabledLayers();

		static constexpr bool Enabled()
		{
			#if _DEBUG
			return true;
			#else
			return false;
			#endif
		}
	};
}

#endif