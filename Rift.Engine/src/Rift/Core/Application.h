#ifndef RIFT_APPLICATION_H
#define RIFT_APPLICATION_H

#include "Rift/Core/Base.h"
#include "Rift/Core/Window.h"
#include "Rift/Core/LayerStack.h"

#include "Rift/Events/Event.h"
#include "Rift/Events/ApplicationEvent.h"

namespace Rift
{
	class RIFT_API RIFT_DECLSPEC Application
	{
	private:
		static Application* _pApplication;
		bool OnWindowClose(WindowCloseEvent& e);
	private:
		Ref<_RFT Window> _pWindow;
		LayerStack _Layers;
		bool _Active;
	public:
		Application();
		Application(_In_ const WindowInfo& info);
		Application(_In_ const Application&) = delete;
		Application(_In_ Application&& other) = delete;
		~Application() noexcept;

		void Run();
		void OnEvent(Event& e);

		void PushLayer(const Ref<Layer>& layer) noexcept;
		void PushOverlay(const Ref<Layer>& overlay) noexcept;
		void PopLayer(const Ref<Layer>& layer) noexcept;
		void PopOverlay(const Ref<Layer>& overlay) noexcept;

		Ref<Window> GetWindow() const noexcept { return _pWindow; }

		static Application& Get() noexcept { return *_pApplication; }
	};
}

#endif // !RIFT_APPLICATION_H