#ifndef RIFT_LAYER_STACK_H
#define RIFT_LAYER_STACK_H

#include <vector>

#include "Rift/Core/Base.h"
#include "Rift/Core/Layer.h"

namespace Rift
{
	class RIFT_API RIFT_DECLSPEC LayerStack
	{
	private:
		std::vector<std::shared_ptr<Layer>> m_Layers;
		uint32_t m_LayerInsertIndex = 0;
	public:
		LayerStack() noexcept = default;
		~LayerStack() noexcept;

		void PushLayer(const std::shared_ptr<Layer>& layer) noexcept;
		void PushOverlay(const std::shared_ptr<Layer>& overlay) noexcept;
		void PopLayer(const std::shared_ptr<Layer>& layer) noexcept;
		void PopOverlay(const std::shared_ptr<Layer>& overlay) noexcept;

		std::vector<std::shared_ptr<Layer>>::iterator begin() { return m_Layers.begin(); }
		std::vector<std::shared_ptr<Layer>>::iterator end() { return m_Layers.end(); }
		std::vector<std::shared_ptr<Layer>>::reverse_iterator rbegin() { return m_Layers.rbegin(); }
		std::vector<std::shared_ptr<Layer>>::reverse_iterator rend() { return m_Layers.rend(); }

		std::vector<std::shared_ptr<Layer>>::const_iterator begin() const { return m_Layers.begin(); }
		std::vector<std::shared_ptr<Layer>>::const_iterator end()	const { return m_Layers.end(); }
		std::vector<std::shared_ptr<Layer>>::const_reverse_iterator rbegin() const { return m_Layers.rbegin(); }
		std::vector<std::shared_ptr<Layer>>::const_reverse_iterator rend() const { return m_Layers.rend(); }
	};
}

#endif /// !RIFT_LAYER_STACK_H