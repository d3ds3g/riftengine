#pragma once

#include "Rift/Core/Base.h"

#pragma warning(push, 0)
#include <spdlog/spdlog.h>
#include <spdlog/fmt/ostr.h>
#pragma warning(pop)


namespace Rift 
{
	class RIFT_API RIFT_DECLSPEC Log
	{
	public:
		static void Init();

		static Ref<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; }
		static Ref<spdlog::logger>& GetClientLogger() { return s_ClientLogger; }
	private:
		static Ref<spdlog::logger> s_CoreLogger;
		static Ref<spdlog::logger> s_ClientLogger;
	};

}

// Core log macros
#define RF_CORE_TRACE(...)    ::Rift::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define RF_CORE_INFO(...)     ::Rift::Log::GetCoreLogger()->info(__VA_ARGS__)
#define RF_CORE_WARN(...)     ::Rift::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define RF_CORE_ERROR(...)    ::Rift::Log::GetCoreLogger()->error(__VA_ARGS__)
#define RF_CORE_CRITICAL(...) ::Rift::Log::GetCoreLogger()->critical(__VA_ARGS__)

// Client log macros
#define RF_TRACE(...)         ::Rift::Log::GetClientLogger()->trace(__VA_ARGS__)
#define RF_INFO(...)          ::Rift::Log::GetClientLogger()->info(__VA_ARGS__)
#define RF_WARN(...)          ::Rift::Log::GetClientLogger()->warn(__VA_ARGS__)
#define RF_ERROR(...)         ::Rift::Log::GetClientLogger()->error(__VA_ARGS__)
#define RF_CRITICAL(...)      ::Rift::Log::GetClientLogger()->critical(__VA_ARGS__)