#ifndef RIFT_LAYER_H
#define RIFT_LAYER_H

#include "Rift/Core/Base.h"
#include "Rift/Events/Event.h"

namespace Rift
{
    class RIFT_API RIFT_DECLSPEC Layer
    {
    public:
        Layer() = default;
        virtual ~Layer() noexcept = default;

        virtual void OnAttach() {}
        virtual void OnEvent(Event& e) {}
        virtual void OnDetach() {}
        virtual void OnUpdate() {}
    };
}

#endif /// !RIFT_LAYER_H