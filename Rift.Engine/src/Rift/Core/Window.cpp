#include "Rift/Core/Window.h"

#include "Rift/Events/ApplicationEvent.h"
#include "Rift/Events/KeyboardEvent.h"
#include "Rift/Events/MouseEvent.h"

#ifdef RIFT_PLATFORM_WINDOWS
#include <GLFW/glfw3.h>
#endif

#include <iostream>

namespace Rift
{
	#ifdef RIFT_PLATFORM_WINDOWS

	struct RIFT_API WindowAPI
	{
		bool Initialized = false;

		WindowAPI()
		{
			if (glfwInit())
				Initialized = true;
		}

		~WindowAPI()
		{
			glfwTerminate();
		}
	};

	static WindowAPI _WindowAPI;


	Window::Window()
		:_Data(), _pHandle(nullptr)
	{
		RequiresVersion<0, 1>();
		_Data.iWidth = 1280;
		_Data.iHeight = 720;
		_Title = "Rift Engine";

		if (!_WindowAPI.Initialized)
			return;

		_pHandle = glfwCreateWindow(_Data.iWidth, _Data.iHeight, _Title.c_str(), nullptr, nullptr);
		glfwMakeContextCurrent(_pHandle);

		_pGFXContext = GraphicsContext::Create();
		_pGFXContext->Init(_pHandle);

		this->SetVsync(true);
		this->InitializeCallbacks();
		_Bitset[_ActiveBit] = true;
	}

	Window::Window(_In_ const WindowInfo& info)
	{
		RequiresVersion<0, 1>();
		_Data.iWidth = info.iWidth;
		_Data.iHeight = info.iHeight;
		_Title = info.Title;

		if (!_WindowAPI.Initialized)
			return;

		_pHandle = glfwCreateWindow(_Data.iWidth, _Data.iHeight, _Title.c_str(), nullptr, nullptr);
		glfwMakeContextCurrent(_pHandle);

		_pGFXContext = GraphicsContext::Create();
		_pGFXContext->Init(_pHandle);

		this->SetVsync(true);
		this->InitializeCallbacks();
		_Bitset[_ActiveBit] = true;
	}

	Window::Window(_In_ const Window& other)
	{
		RequiresVersion<0, 1>();
		_Data.iWidth = other._Data.iWidth;
		_Data.iHeight = other._Data.iHeight;
		_Title = other._Title;
		if (!_WindowAPI.Initialized)
			return;

		_pHandle = glfwCreateWindow(_Data.iWidth, _Data.iHeight, _Title.c_str(), nullptr, nullptr);
		glfwMakeContextCurrent(_pHandle);

		_pGFXContext = GraphicsContext::Create();
		_pGFXContext->Init(_pHandle);

		this->SetVsync(true);
		this->InitializeCallbacks();
		_Bitset[_ActiveBit] = true;
	}

	Window::Window(_In_ Window&& other) noexcept
		: _Title(std::move(other._Title))
	{
		RequiresVersion<0, 1>();
		_Data.iWidth = other._Data.iWidth;
		_Data.iHeight = other._Data.iHeight;
		_pHandle = other._pHandle;

		other._Data.iWidth = 0;
		other._Data.iHeight = 0;
		other._pHandle = nullptr;

		_Bitset[_ActiveBit] = true;
		this->SetVsync(true);
		other._Bitset[_ActiveBit] = false;
		other._Bitset[_VSyncBit] = false;
	}

	void Window::InitializeCallbacks()
	{

		glfwSetWindowUserPointer(_pHandle, &_Data);
		glfwSetWindowSizeCallback(_pHandle, [](GLFWwindow* window, int width, int height)
		{
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
			data.iWidth = width;
			data.iHeight = height;

			WindowResizeEvent event(width, height);
			data.pEventCallback(event);
		});

		glfwSetWindowCloseCallback(_pHandle, [](GLFWwindow* window)
		{
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);
			WindowCloseEvent event;
			data.pEventCallback(event);
		});

		glfwSetKeyCallback(_pHandle, [](GLFWwindow* window, int key, int scancode, int action, int mods)
		{
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

			switch (action)
			{
			case GLFW_PRESS:
			{
				KeyPressedEvent event(key, 0);
				data.pEventCallback(event);
				break;
			}
			case GLFW_RELEASE:
			{
				KeyReleasedEvent event(key);
				data.pEventCallback(event);
				break;
			}
			case GLFW_REPEAT:
			{
				KeyPressedEvent event(key, 1);
				data.pEventCallback(event);
				break;
			}
			}
		});

		glfwSetCharCallback(_pHandle, [](GLFWwindow* window, unsigned int keycode)
		{
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

			KeyTypedEvent event(keycode);
			data.pEventCallback(event);
		});

		glfwSetMouseButtonCallback(_pHandle, [](GLFWwindow* window, int button, int action, int mods)
		{
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

			switch (action)
			{
			case GLFW_PRESS:
			{
				MouseButtonPressedEvent event(button);
				data.pEventCallback(event);
				break;
			}
			case GLFW_RELEASE:
			{
				MouseButtonReleasedEvent event(button);
				data.pEventCallback(event);
				break;
			}
			}
		});

		glfwSetScrollCallback(_pHandle, [](GLFWwindow* window, double xOffset, double yOffset)
		{
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

			MouseScrolledEvent event((float)xOffset, (float)yOffset);
			data.pEventCallback(event);
		});

		glfwSetCursorPosCallback(_pHandle, [](GLFWwindow* window, double xPos, double yPos)
		{
			WindowData& data = *(WindowData*)glfwGetWindowUserPointer(window);

			MouseMovedEvent event((float)xPos, (float)yPos);
			data.pEventCallback(event);
		});
	}

	Window::~Window()
	{
		if (_pHandle)
			glfwDestroyWindow(_pHandle);
	}

	void Window::OnUpdate()
	{
		_Bitset[_ActiveBit] = !glfwWindowShouldClose(_pHandle);
		glfwSwapBuffers(_pHandle);
		glfwPollEvents();
	}

	void Window::SetEventCallback(const std::function<void(Event& e)>& callback)
	{
		_Data.pEventCallback = callback;
	}

	bool Window::IsActive() noexcept
	{
		return _Bitset[_ActiveBit];
	}

	bool Window::IsVsync() noexcept
	{
		return _Bitset[_VSyncBit];
	}

	void Window::SetVsync(bool vsync) noexcept
	{
		glfwSwapInterval(vsync);
		_Bitset[_VSyncBit] = vsync;
	}

	#endif
}
