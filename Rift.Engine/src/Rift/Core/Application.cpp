#include "Rift/Core/Application.h"
#include "Rift/Core/Log.h"

#include "Rift/Events/KeyboardEvent.h"
#include "Rift/Events/MouseEvent.h"

namespace Rift
{
	Application* Application::_pApplication = nullptr;

	bool Application::OnWindowClose(WindowCloseEvent& e)
	{
		_Active = false;
		return true;
	}

	Application::Application()
	{
		if (_pApplication != nullptr)
			return;

		Log::Init();

		_pWindow = CreateRef<Window>();
		_pWindow->SetEventCallback(RF_BIND_EVENT_FN(&Application::OnEvent));
		_pApplication = this;
	}

	Application::Application(_In_ const WindowInfo& info)
	{
		if (_pApplication != nullptr)
			return;

		Log::Init();

		_pWindow = CreateScope<Window>(info);
		_pWindow->SetEventCallback(RF_BIND_EVENT_FN(&Application::OnEvent));
		_pApplication = this;
	}

	Application::~Application() noexcept
	{
	}

	void Application::PushLayer(const Ref<Layer>& layer) noexcept
	{
		_Layers.PushLayer(layer);
		layer->OnAttach();
	}

	void Application::PushOverlay(const Ref<Layer>& overlay) noexcept
	{
		_Layers.PushOverlay(overlay);
		overlay->OnAttach();
	}

	void Application::PopLayer(const Ref<Layer>& layer) noexcept
	{
		_Layers.PopLayer(layer);
		layer->OnAttach();
	}

	void Application::PopOverlay(const Ref<Layer>& overlay) noexcept
	{
		_Layers.PopOverlay(overlay);
		overlay->OnAttach();
	}

	void Application::Run()
	{
		while (_Active)
		{
			for (auto& l : _Layers)
			{
				l->OnUpdate();
			}

			_pWindow->OnUpdate();
		}
	}

	void Application::OnEvent(Event& e)
	{
		EventDispatcher dispatcher(e);
		dispatcher.Dispatch<WindowCloseEvent>(RF_BIND_EVENT_FN(&Application::OnWindowClose));
		//dispatcher.Dispatch<WindowResizeEvent>(RF_BIND_EVENT_FN(Application::OnWindowResize));

		for (auto it = _Layers.rbegin(); it != _Layers.rend(); ++it)
		{
			if (e.Handled)
				break;
			(*it)->OnEvent(e);
		}
	}
}

