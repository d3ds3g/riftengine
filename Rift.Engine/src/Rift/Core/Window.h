#ifndef RIFT_WINDOW_H
#define RIFT_WINDOW_H

#include "Rift/Core/Base.h"
#include "Rift/Events/Event.h"
#include "Rift/GFX/GraphicsContext.h"

#include <string>
#include <functional>
#include <bitset>

#ifdef RIFT_PLATFORM_WINDOWS
using WindowHandle = struct GLFWwindow;
#endif

namespace Rift
{
	struct RIFT_API WindowInfo
	{
		int							   iWidth = 1280;
		int							   iHeight = 720;
		std::string					   Title = "Rift Engine";
	};

	class RIFT_API RIFT_DECLSPEC Window
	{
	private:
		struct WindowData
		{
			int iWidth;
			int iHeight;
			std::function<void(Event& e)> pEventCallback;
		};

		static constexpr uint8_t _ActiveBit = 0;
		static constexpr uint8_t _VSyncBit = 1;

		void InitializeCallbacks();
	private:
		WindowHandle* _pHandle;
		WindowData _Data;
		std::string _Title;
		std::bitset<16> _Bitset;
		Ref<GraphicsContext> _pGFXContext;
	public:
		Window();
		Window(_In_ const WindowInfo& info);
		Window(_In_ const Window& other);
		Window(_In_ Window&& other) noexcept;
		~Window() noexcept;

		void OnUpdate();

		void SetEventCallback(const std::function<void(Event& e)>& callback);

		bool IsActive() noexcept;
		bool IsVsync() noexcept;

		void SetVsync(bool vsync) noexcept;
		void* RawWindow() const noexcept { return _pHandle; }

		constexpr std::pair<int, int> Size() const noexcept { return std::make_pair(_Data.iWidth, _Data.iHeight); }
	};
}



#endif // !RIFT_WINDOW_H
