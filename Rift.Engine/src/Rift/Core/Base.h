#ifndef RIFT_BASE_H
#define RIFT_BASE_H

#pragma region Basic includes

#include <stdint.h>
#include <memory>
#include <sal.h>

#pragma endregion

#pragma region API Macros

#ifdef RIFT_CORE
#define RIFT_DECLSPEC __declspec(dllexport)
#else
#define RIFT_DECLSPEC __declspec(dllimport)
#endif

#define RIFT_API

#define RIFT_EXTERN_BEGIN extern "C"{
#define RIFT_EXTERN_END }

#define _RFT Rift::
#define RIFT_CORE_ASSERT(x, y) if(!(x)) {RF_CORE_ERROR(y); RF_BREAK;}
#define RIFT_ASSERT(x, y) if(!x) {RF_ERROR(y); RF_BREAK;}
#define RF_BIND_EVENT_FN(x) std::bind(x, this, std::placeholders::_1)

namespace Rift
{
	constexpr uint8_t VersionMajor = 0;
	constexpr uint8_t VersionMinor = 1;

	template <uint8_t major, uint8_t minor>
	constexpr void RequiresVersion()
	{
		static_assert(VersionMajor <= major, "Version major is outdated!");
		static_assert(VersionMinor <= minor, "Version minor is outdated!");
	}
}

#pragma endregion


#pragma region Platform Macros

#ifdef _WIN32
#define RIFT_PLATFORM_WINDOWS
#define RF_BREAK __debugbreak()
#endif
#ifdef __unix
#define RIFT_PLATFORM_UNIX
#endif
#ifdef __linux__
#define RIFT_PLATFORM_LINUX
#endif
#ifdef __APPLE__
#define RIFT_PLATFORM_MACOS
#endif
#ifdef __FreeBSD__
#define RIFT_PLATFORM_FREEBSD
#endif 
#ifdef __ANDROID__
#define RIFT_PLATFORM_ANDROID



#endif

#pragma endregion


#pragma region Types

namespace Rift
{
	template<typename T>
	using Scope = std::unique_ptr<T>;
	template<typename T, typename ... Args>
	constexpr Scope<T> CreateScope(Args&& ... args)
	{
		return std::make_unique<T>(std::forward<Args>(args)...);
	}

	template<typename T>
	using Ref = std::shared_ptr<T>;
	template<typename T, typename ... Args>
	constexpr Ref<T> CreateRef(Args&& ... args)
	{
		return std::make_shared<T>(std::forward<Args>(args)...);
	}
}

#pragma endregion


#endif // !RIFT_BASE_H
