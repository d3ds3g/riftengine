#include "Rift/Core/LayerStack.h"

namespace Rift
{
	LayerStack::~LayerStack() noexcept
	{
		for (auto& l : m_Layers)
		{
			l->OnDetach();
		}
	}

	void LayerStack::PushLayer(const std::shared_ptr<Layer>& layer) noexcept
	{
		m_Layers.emplace(m_Layers.begin() + m_LayerInsertIndex, layer);
		m_LayerInsertIndex++;
	}

	void LayerStack::PushOverlay(const std::shared_ptr<Layer>& overlay) noexcept
	{
		m_Layers.emplace_back(overlay);
	}

	void LayerStack::PopLayer(const std::shared_ptr<Layer>& layer) noexcept
	{
		auto it = std::find(m_Layers.begin(), m_Layers.begin() + m_LayerInsertIndex, layer);
		if (it != m_Layers.begin() + m_LayerInsertIndex)
		{
			layer->OnDetach();
			m_Layers.erase(it);
			m_LayerInsertIndex--;
		}
	}

	void LayerStack::PopOverlay(const std::shared_ptr<Layer>& overlay) noexcept
	{
		auto it = std::find(m_Layers.begin() + m_LayerInsertIndex, m_Layers.end(), overlay);
		if (it != m_Layers.end())
		{
			overlay->OnDetach();
			m_Layers.erase(it);
		}
	}

}