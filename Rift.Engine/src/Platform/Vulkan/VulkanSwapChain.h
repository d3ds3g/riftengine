#ifndef RIFT_VULKAN_SWAP_CHAIN_H
#define RIFT_VULKAN_SWAP_CHAIN_H

#include "Rift/Core/Base.h"
#include "Rift/GFX/SwapChain.h"

#include <vulkan/vulkan.h>
#include <vector>

typedef struct GLFWwindow GLFWwindow;

namespace Rift
{
	struct RIFT_API VulkanSwapChainSupport
	{
		VkSurfaceCapabilitiesKHR Capabilities;
		std::vector<VkSurfaceFormatKHR> Formats;
		std::vector<VkPresentModeKHR> PresentModes;
	};

	class RIFT_API RIFT_DECLSPEC VulkanSwapChain : public SwapChain
	{
	private:
		static VkSurfaceFormatKHR ChooseFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
		static VkPresentModeKHR ChoosePresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);
		static VkExtent2D ChooseSwapExtent(GLFWwindow* pWindow, const VkSurfaceCapabilitiesKHR& capabilities);
	private:
		VkSwapchainKHR _SwapChain;
		std::vector<VkImage> _Images;
		std::vector<VkImageView> _ImageViews;
		VkFormat _Format;
		VkExtent2D _Extent2D;
	public:
		VulkanSwapChain(void* pWinHandle);
		~VulkanSwapChain() noexcept;
		
		std::pair<uint32_t, uint32_t> Size() override { return std::make_pair(_Extent2D.width, _Extent2D.height); }
	};
}

#endif // !RIFT_SWAP_CHAIN_H
