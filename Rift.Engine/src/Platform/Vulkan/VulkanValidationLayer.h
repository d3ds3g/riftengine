#ifndef RIFT_VULKAN_VALIDATION_LAYER_H
#define RIFT_VULKAN_VALIDATION_LAYER_H

#include "Rift/Core/Base.h"
#include "Rift/GFX/ValidationLayer.h"

#include <vector>
#include <vulkan/vulkan.h>

namespace Rift
{
	class RIFT_API RIFT_DECLSPEC VulkanValidationLayer : public ValidationLayer
	{
	private:
		std::vector<const char*> _EnabledLayers;
		std::vector<VkLayerProperties> _AvailibleLayers;
	public:
		VulkanValidationLayer();

		void ImplAttach(const char* pLayerName) override;

		uint32_t ImplEnabledLayersCount() override { return _EnabledLayers.size(); }
		const char** ImplEnabledLayers() override { return _EnabledLayers.data(); }
	};
}

#endif