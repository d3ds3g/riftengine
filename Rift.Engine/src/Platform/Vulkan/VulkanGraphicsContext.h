#ifndef RIFT_VULKAN_GRAPHICS_CONTEXT_H
#define RIFT_VULKAN_GRAPHICS_CONTEXT_H

#include "Rift/Core/Base.h"
#include "Rift/GFX/GraphicsContext.h"
#include "Rift/GFX/GraphicsDevice.h"
#include "Rift/GFX/GraphicsSurface.h"
#include "Rift/GFX/SwapChain.h"

#ifdef RIFT_PLATFORM_WINDOWS
using WindowHandle = struct GLFWwindow;
#endif

#include <vulkan/vulkan.h>

namespace Rift
{
	class RIFT_API RIFT_DECLSPEC VulkanGraphicsContext : public GraphicsContext
	{
	private:
		WindowHandle * _Window;
		VkInstance _Instance;
		VkDebugUtilsMessengerEXT _DebugMessenger;
		
		Ref<SwapChain> _SwapChain;
		Ref<GraphicsSurface> _Surface;
		Ref<GraphicsPhysicalDevice> _PhysicalDevice;
		Ref<GraphicsLogicalDevice> _LogicalDevice;
	public:
		~VulkanGraphicsContext() noexcept;
		void Init(void* pWinHandle) override;
		uint32_t ExtensionCount() override;

		void* RawContext() noexcept override { return _Instance; }
	};
}

#endif // !RIFT_RENDERER_API_H
