#include "VulkanShader.h"
#include "Rift/Core/Log.h"
#include "Rift/Core/Application.h"

#include "Rift/GFX/SwapChain.h"
#include "Rift/GFX/GraphicsDevice.h"
#include "Rift/GFX/GraphicsContext.h"

#include <vector>

namespace Rift
{
	VulkanShader::VulkanShader(const ShaderInfo& info)
	{
		RIFT_CORE_ASSERT(info.FileMode, "Current platform does not support non-file shader input.");

		VkDevice logicalDevice = (VkDevice)GraphicsLogicalDevice::Get()->GetRawDevice();
		VkShaderModule vertexModule;
	
		std::ifstream vsFile(info.VertexShaderFile, std::ios::ate | std::ios::binary);
		RIFT_CORE_ASSERT(vsFile.is_open(), "Could not find file for the vertex shader.");
		size_t vsFileSize = (size_t)vsFile.tellg();

		std::vector<char> vsFileContent(vsFileSize);
		vsFile.seekg(0);
		vsFile.read(vsFileContent.data(), vsFileSize);

		VkShaderModuleCreateInfo vsCreateInfo{
			.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
			.codeSize = vsFileSize,
			.pCode = reinterpret_cast<uint32_t*>(vsFileContent.data()),
		};

		RIFT_CORE_ASSERT(vkCreateShaderModule(logicalDevice, &vsCreateInfo, nullptr, &vertexModule) == VK_SUCCESS, 
			"Vertex shader module creation failed.");
		vsFile.close();

		VkShaderModule fragmentModule;
		std::ifstream fsFile(info.FragmentShaderFile, std::ios::ate | std::ios::binary);
		RIFT_CORE_ASSERT(fsFile.is_open(), "Could not find file for the fragment shader.");
		size_t fsFileSize = (size_t)fsFile.tellg();

		std::vector<char> fsFileContent(fsFileSize);
		fsFile.seekg(0);
		fsFile.read(fsFileContent.data(), fsFileSize);

		VkShaderModuleCreateInfo fsCreateInfo{
			.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
			.codeSize = vsFileSize,
			.pCode = reinterpret_cast<uint32_t*>(fsFileContent.data())
		};

		RIFT_CORE_ASSERT(vkCreateShaderModule(logicalDevice, &fsCreateInfo, nullptr, &fragmentModule) == VK_SUCCESS, 
			"Fragment shader module creation failed.");
		fsFile.close();

		VkPipelineShaderStageCreateInfo vsStageCreateInfo{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
			.stage = VK_SHADER_STAGE_VERTEX_BIT,
			.module = vertexModule,
			.pName = "main",
		};

		VkPipelineShaderStageCreateInfo fsStageCreateInfo{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
			.stage = VK_SHADER_STAGE_FRAGMENT_BIT,
			.module = fragmentModule,
			.pName = "main",
		};

		std::array<VkPipelineShaderStageCreateInfo> shaderStages = {vsStageCreateInfo, fsStageCreateInfo};

		VkPipelineVertexInputStateCreateInfo vertexInputInfo{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
			.vertexBindingDescriptionCount = 0,
			.pVertexBindingDescriptions = nullptr,
			.vertexAttributeDescriptionCount = 0,
			.pVertexAttributeDescriptions = nullptr
		};

		VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
			.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
			.primitiveRestartEnable = VK_FALSE
		};

		SwapChain* swapChain = SwapChain::Get();
		auto [iExtentWidth, iExtentHeight] = swapChain->Size();
		VkViewport viewport{
			.x = 0.0f,
			.y = 0.0f,
			.width = (float)iExtentWidth,
			.height = (float)iExtentHeight,
			.minDepth = 0.0f,
			.maxDepth = 1.0f
		};
		VkExtent2D swapChainExtent;
		swapChainExtent.width = iExtentWidth;
		swapChainExtent.height = iExtentHeight;

		VkRect2D scissor{};
		scissor.offset = { 0, 0 };
		scissor.extent = swapChainExtent;

		VkPipelineViewportStateCreateInfo viewportStateInfo{
			.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
			.viewportCount = 1,
			.pViewports = &viewport,
			.scissorCount = 1,
			.pScissors = &scissor
		};


		vkDestroyShaderModule(logicalDevice, vertexModule, nullptr);
		vkDestroyShaderModule(logicalDevice, fragmentModule, nullptr);
	}

	VulkanShader::~VulkanShader() noexcept
	{
	}

}

