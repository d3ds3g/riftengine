#ifndef RIFT_VULKAN_GFXDEVICE_H
#define RIFT_VULKAN_GFXDEVICE_H

#include "Rift/Core/Base.h"
#include "Rift/GFX/GraphicsDevice.h"

#include <vulkan/vulkan.h>
#include <array>

namespace Rift
{
	/// <summary>
	/// Represents a handle to a physical graphics device(GPU). For now Rift supports only one GPU per program,
	/// but Nvidia's SLI and AMD's Crossfire will be implemented in the far future. This is the reason the first
	/// instance of this class is set to Rift::GraphicsPhysicalDevice's _pSingletonInstance. Any further instantiation of
	/// the class may result in crashes or unexpected behavior. Instantiate this class ONLY if you plan to use JUST the cross-API abstraction
	/// of the GFX API.
	/// </summary>
	class RIFT_API RIFT_DECLSPEC VulkanGraphicsPhysicalDevice final : public GraphicsPhysicalDevice
	{
	private:
		/// <summary>
		/// Rates the device's capabilities by the supported texture quality(the higher, the better).
		/// Also checks if the device supports required features(geometry shader), queue families(graphics
		/// and present) and surface support. The score is 0 if the device is not suitable.
		/// </summary>
		/// <param name="device:">Any valid VkPhysicalDevice.</param>
		/// <returns>The score.</returns>
		static uint32_t RateDeviceCapabilities(_In_ VkPhysicalDevice device) noexcept;

		/// <summary>
		/// The function finds the required queue families and their index. Required queue families at this point are the graphics
		/// and the present queue families. Their presence guarantees that actual graphics can be rendered to the surface.
		/// Used in Rift::VulkanGraphicsPhysicalDevice::RateDeviceCapabilities to determine if the GPU is suitable.
		/// </summary>
		/// <param name="device:">Any valid VkPhysicalDevice.</param>
		/// <param name="indices:">Reference to a QueueFamiliesIndices for the information to be written in.</param>
		static void FindQueueFamilies(_In_ VkPhysicalDevice device, _Out_ QueueFamiliesIndices& indices);

		/// <summary>
		/// Check is the grahpics physical device supports the following extension/s: "VK_KHR_swapchain".
		/// Used in Rift::VulkanGraphicsPhysicalDevice::RateDeviceCapabilities to determine if the GPU is suitable.
		/// </summary>
		/// <param name="device:">Any valid VkPhysicalDevice.</param>
		/// <returns>True if the required extensions are supported.</returns>
		static bool CheckRequiredExtensionsSupport(_In_ VkPhysicalDevice device);

		static constexpr std::array<const char*, 1> _RequiredExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
	private:
		VkPhysicalDevice _PhysicalDevice = VK_NULL_HANDLE;
		VkPhysicalDeviceProperties _Properties;
		VkPhysicalDeviceFeatures _Features;
		QueueFamiliesIndices _QueuesSupported;
	public:
		VulkanGraphicsPhysicalDevice();
		~VulkanGraphicsPhysicalDevice() noexcept;

		void* GetRawDevice() override;
		QueueFamiliesIndices& QueuesSupported() noexcept override { return _QueuesSupported; }
	};

	class RIFT_API RIFT_DECLSPEC VulkanGraphicsLogicalDevice : public GraphicsLogicalDevice
	{
	private:
		static constexpr std::array<const char*, 1> _RequiredExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
	private:
		VkDevice _LogicalDevice = VK_NULL_HANDLE;
		VkQueue _GraphicsQueue;
		VkQueue _PresentQueue;
	public:
		VulkanGraphicsLogicalDevice();
		~VulkanGraphicsLogicalDevice() noexcept;

		void* GetRawDevice() override;
	};
}

#endif