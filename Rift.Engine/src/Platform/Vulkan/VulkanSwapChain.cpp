#include "Platform/Vulkan/VulkanSwapChain.h"
#include "Rift/Core/Application.h"
#include "Rift/Core/Log.h"

#include "Rift/GFX/GraphicsSurface.h"
#include "Rift/GFX/GraphicsDevice.h"
#include "Rift/GFX/GraphicsContext.h"

#include <GLFW/glfw3.h>
#include <algorithm>

namespace Rift
{
	VkSurfaceFormatKHR VulkanSwapChain::ChooseFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats)
	{
		for (const auto& availableFormat : availableFormats)
		{
			if (availableFormat.format == VK_FORMAT_B8G8R8A8_SRGB && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
			{
				return availableFormat;
			}
		}

		return availableFormats[0];
	}

	VkPresentModeKHR VulkanSwapChain::ChoosePresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes)
	{
		for (const auto& availablePresentMode : availablePresentModes) 
		{
			if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR)
			{
				return availablePresentMode;
			}
		}

		return VK_PRESENT_MODE_FIFO_KHR;
	}

	VkExtent2D VulkanSwapChain::ChooseSwapExtent(GLFWwindow* pWindow, const VkSurfaceCapabilitiesKHR& capabilities)
	{
		if (capabilities.currentExtent.width != UINT32_MAX)
			return capabilities.currentExtent;
		
		int32_t width, height;
		glfwGetFramebufferSize(pWindow, &width, &height);

		VkExtent2D rExtent = {
			(uint32_t)width,
			(uint32_t)height
		};

		rExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.minImageExtent.width, rExtent.width));
		rExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.minImageExtent.height, rExtent.height));

		return rExtent;
	}

	VulkanSwapChain::VulkanSwapChain(void* pWinHandle)
	{
		VkPhysicalDevice device = (VkPhysicalDevice)GraphicsPhysicalDevice::Get()->GetRawDevice();
		VkDevice logicalDevice = (VkDevice)GraphicsLogicalDevice::Get()->GetRawDevice();
		VulkanSwapChainSupport support;
		VkSurfaceKHR surface = *(uint64_t*)GraphicsSurface::Get()->RawSurface();

		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &support.Capabilities);

		uint32_t uFormatCount = 0;
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &uFormatCount, nullptr);

		support.Formats.resize(uFormatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &uFormatCount, support.Formats.data());

		uint32_t uPresentCount = 0;
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &uPresentCount, nullptr);

		support.PresentModes.resize(uPresentCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &uPresentCount, support.PresentModes.data());

		auto surfaceFormat = ChooseFormat(support.Formats);
		auto presentMode = ChoosePresentMode(support.PresentModes);
		auto extent = ChooseSwapExtent(static_cast<GLFWwindow*>(pWinHandle), support.Capabilities);

		uint32_t imageCount = support.Capabilities.minImageCount + 1;

		RIFT_CORE_ASSERT((support.Capabilities.maxImageCount > 0 && imageCount < support.Capabilities.maxImageCount),
			"Driver did/could not return requested image count for the swapchain.");

		VkSwapchainCreateInfoKHR createInfo{
			.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
			.surface = surface,
			.minImageCount = imageCount,
			.imageFormat = surfaceFormat.format,
			.imageColorSpace = surfaceFormat.colorSpace,
			.imageExtent = extent,
			.imageArrayLayers = 1,
			.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
			.preTransform = support.Capabilities.currentTransform,
			.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
			.presentMode = presentMode,
			.clipped = VK_TRUE,
			.oldSwapchain = VK_NULL_HANDLE
		};

		QueueFamiliesIndices& indices = GraphicsPhysicalDevice::Get()->QueuesSupported();
		uint32_t aIndices[] = { indices.GraphicsFamily.value(), indices.PresentFamily.value() };
		if (indices.GraphicsFamily != indices.PresentFamily)
		{
			createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
			createInfo.queueFamilyIndexCount = 2;
			createInfo.pQueueFamilyIndices = aIndices;
		}
		else
		{
			createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
			createInfo.queueFamilyIndexCount = 0;
			createInfo.pQueueFamilyIndices = nullptr;
		}

		RIFT_CORE_ASSERT(vkCreateSwapchainKHR(logicalDevice, &createInfo, nullptr, &_SwapChain) == VK_SUCCESS,
			"Could not create the swapchain.");

		uint32_t uFinalImageCount = 0;
		vkGetSwapchainImagesKHR(logicalDevice, _SwapChain, &uFinalImageCount, nullptr);
		_Images.resize(uFinalImageCount);
		vkGetSwapchainImagesKHR(logicalDevice, _SwapChain, &uFinalImageCount, _Images.data());

		_Format = surfaceFormat.format;
		_Extent2D = extent;

		// Setup image views

		_ImageViews.resize(_Images.size());
		for (uint32_t i = 0; i < _Images.size(); ++i)
		{
			VkImageViewCreateInfo imageViewCreateInfo{
				.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
				.image = _Images[i],
				.viewType = VK_IMAGE_VIEW_TYPE_2D,
				.format = _Format,
			};

			imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
			imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

			imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
			imageViewCreateInfo.subresourceRange.levelCount = 1;
			imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
			imageViewCreateInfo.subresourceRange.layerCount = 1;

			RIFT_CORE_ASSERT(vkCreateImageView(logicalDevice, &imageViewCreateInfo, nullptr, &_ImageViews[i]) == VK_SUCCESS, "Could not create image views!");
		}
	}

	VulkanSwapChain::~VulkanSwapChain() noexcept
	{
		VkDevice logicalDevice = (VkDevice)GraphicsLogicalDevice::Get()->GetRawDevice();
		for (auto& view : _ImageViews)
			vkDestroyImageView(logicalDevice, view, nullptr);

		vkDestroySwapchainKHR(logicalDevice, _SwapChain, nullptr);
	}
}