#include "Platform/Vulkan/VulkanGraphicsContext.h"
#include "Rift/Core/Log.h"

#ifdef RIFT_PLATFORM_WINDOWS
#include <GLFW/glfw3.h>
#endif

namespace Rift
{
	static VKAPI_ATTR VkBool32 VKAPI_CALL vulkanDebugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData)
	{
		switch (messageSeverity)
		{
		case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
			RF_CORE_INFO(pCallbackData->pMessage);
			break;
		case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
			RF_CORE_INFO(pCallbackData->pMessage);
			break;
		case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
			RF_CORE_WARN(pCallbackData->pMessage);
			break;
		case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
			RF_CORE_ERROR(pCallbackData->pMessage);
			break;
		default:
			break;
		}

		return VK_FALSE;
	}

	static VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) {
		auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
		if (func != nullptr) {
			return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
		}
		else {
			return VK_ERROR_EXTENSION_NOT_PRESENT;
		}
	}

	static void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator) {
		auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
		if (func != nullptr) {
			func(instance, debugMessenger, pAllocator);
		}
	}

	VulkanGraphicsContext::~VulkanGraphicsContext() noexcept
	{
		_SwapChain.reset();
		_Surface.reset();
		_LogicalDevice.reset();
		_PhysicalDevice.reset();


		if (ValidationLayer::Enabled())
			DestroyDebugUtilsMessengerEXT(_Instance, _DebugMessenger, nullptr);
		vkDestroyInstance(_Instance, nullptr);
	}

	void VulkanGraphicsContext::Init(void* pWinHandle)
	{
		// Create context
		_Window = static_cast<WindowHandle*>(pWinHandle);

		// Init the validation layers
		ValidationLayer::Init();
		ValidationLayer::Attach("VK_LAYER_KHRONOS_validation");

		// Create instance
		VkApplicationInfo appInfo{
			.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
			.pApplicationName = "Sandbox app",
			.applicationVersion = VK_MAKE_VERSION(0, 1, 0),
			.pEngineName = "Rift Engine",
			.engineVersion = VK_MAKE_VERSION(0, 1, 0),
			.apiVersion = VK_VERSION_1_0
		};

		VkInstanceCreateInfo createInfo{
			.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
			.pApplicationInfo = &appInfo,
		};

		uint32_t glfwExtensionCount = 0;
		const char** glfwExtensions;

		glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

		std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

		if (ValidationLayer::Enabled())
		{
			extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

			createInfo.enabledLayerCount = ValidationLayer::EnabledLayersCount();
			createInfo.ppEnabledLayerNames = ValidationLayer::EnabledLayers();
		}
		createInfo.enabledExtensionCount = (uint32_t)extensions.size();
		createInfo.ppEnabledExtensionNames = extensions.data();

		RIFT_CORE_ASSERT(vkCreateInstance(&createInfo, nullptr, &_Instance) == VK_SUCCESS, "VkInstance creation failed!");

		// Init the debug messenger & load the functions
		if (ValidationLayer::Enabled())
		{
			VkDebugUtilsMessengerCreateInfoEXT messengerCreateInfo{
				.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
				.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
				.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
				.pfnUserCallback = vulkanDebugCallback,
				.pUserData = nullptr,
			};

			RIFT_CORE_ASSERT(CreateDebugUtilsMessengerEXT(_Instance, &messengerCreateInfo, nullptr, &_DebugMessenger) == VK_SUCCESS,
				"VkDebugUtilsMessengerCreateInfoEXT creation failed!");

			_Surface = GraphicsSurface::Create(pWinHandle);
			_PhysicalDevice = GraphicsPhysicalDevice::Create();
			_LogicalDevice = GraphicsLogicalDevice::Create();
			_SwapChain = SwapChain::Create(pWinHandle);
		}
	}

	uint32_t VulkanGraphicsContext::ExtensionCount()
	{
		uint32_t extensionCount = 0;
		vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
		return extensionCount;
	}

}
