#ifndef RIFT_VULKAN_SHADER_H
#define RIFT_VULKAN_SHADER_H

#include "Rift/GFX/Shader.h"

#include <vulkan/vulkan.h>

namespace Rift
{
	class RIFT_API RIFT_DECLSPEC VulkanShader final : public Shader
	{
	private:
	public:
		VulkanShader(const ShaderInfo& info);
		~VulkanShader() noexcept;
	};
}

#endif