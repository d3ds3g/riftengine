#include "Platform/Vulkan/VulkanGraphicsSurface.h"
#include "Rift/Core/Log.h"
#include "Rift/GFX/GraphicsContext.h"

#include <GLFW/glfw3.h>
#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3native.h>
#include <vulkan/vulkan_win32.h>

namespace Rift
{
	VulkanGraphicsSurface::VulkanGraphicsSurface(void* pWinHandle)
	{
		// TODO use native window surfaces in the future

		auto context = GraphicsContext::Get();
		VkWin32SurfaceCreateInfoKHR createInfo{
			.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR,
			.hinstance = GetModuleHandle(nullptr),
			.hwnd = glfwGetWin32Window(static_cast<GLFWwindow*>(pWinHandle)),
		};

		RIFT_CORE_ASSERT((vkCreateWin32SurfaceKHR(static_cast<VkInstance>(context->RawContext())
			, &createInfo, nullptr, &_Surface) == VK_SUCCESS),
			"Surface creation failed!");
	}

	VulkanGraphicsSurface::~VulkanGraphicsSurface() noexcept
	{
		auto context = GraphicsContext::Get();
		vkDestroySurfaceKHR(static_cast<VkInstance>(context->RawContext()), _Surface, nullptr);
	}

	void* VulkanGraphicsSurface::RawSurface()
	{
		return &_Surface;
	}

}
