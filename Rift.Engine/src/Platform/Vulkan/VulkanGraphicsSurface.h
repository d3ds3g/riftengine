#ifndef RIFT_VULKAN_GRAPHICS_SURFACE_H
#define RIFT_VULKAN_GRAPHICS_SURFACE_H

#include "Rift/Core/Base.h"
#include "Rift/GFX/GraphicsSurface.h"

#include <vulkan/vulkan.h>

namespace Rift
{
	class RIFT_API RIFT_DECLSPEC VulkanGraphicsSurface final : public GraphicsSurface
	{
	private:
		VkSurfaceKHR _Surface = 0;
	public:
		VulkanGraphicsSurface(void* pWinHandle);
		~VulkanGraphicsSurface() noexcept;

		void* RawSurface() override;
	};
}

#endif // !RIFT_GRAPHICS_SURFACE_H
