#include "Platform/Vulkan/VulkanGraphicsDevice.h"
#include "Rift/Core/Log.h"

#include "Rift/GFX/GraphicsContext.h"
#include "Rift/GFX/GraphicsSurface.h"
#include "Rift/GFX/ValidationLayer.h"

#include <vector>
#include <map>
#include <set>

bool operator==(const VkExtensionProperties& props, const char* extensionName)
{
	return std::strcmp(props.extensionName, extensionName) == 0;
}

namespace Rift
{
	struct RIFT_API VulkanSwapChainSupport
	{
		VkSurfaceCapabilitiesKHR Capabilities;
		std::vector<VkSurfaceFormatKHR> Formats;
		std::vector<VkPresentModeKHR> PresentModes;
	};

	uint32_t VulkanGraphicsPhysicalDevice::RateDeviceCapabilities(_In_ VkPhysicalDevice device) noexcept
	{
		uint32_t uScore = 0;

		VkPhysicalDeviceProperties props;
		VkPhysicalDeviceFeatures features;
		vkGetPhysicalDeviceProperties(device, &props);
		vkGetPhysicalDeviceFeatures(device, &features);

		// Rate device's performance
		if (props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
			uScore += 1000;

		uScore += props.limits.maxImageDimension2D;

		if (!features.geometryShader)
			return 0;

		// Ensure device supports required family queues

		QueueFamiliesIndices indices;
		FindQueueFamilies(device, indices);

		if (!indices)
			return 0;

		// Check for extension support
		if (!CheckRequiredExtensionsSupport(device))
			return 0;

		// Check for swapchain support
		VulkanSwapChainSupport support;
		VkSurfaceKHR surface = *(uint64_t*)GraphicsSurface::Get()->RawSurface();

		vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &support.Capabilities);

		uint32_t uFormatCount = 0;
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &uFormatCount, nullptr);

		if (uFormatCount == 0)
			return 0;
		support.Formats.resize(uFormatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &uFormatCount, support.Formats.data());

		uint32_t uPresentCount = 0;
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &uPresentCount, nullptr);
		if (uPresentCount == 0)
			return 0;
		support.PresentModes.resize(uPresentCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &uPresentCount, support.PresentModes.data());

		return uScore;
	}

	void VulkanGraphicsPhysicalDevice::FindQueueFamilies(_In_ VkPhysicalDevice device, _Out_ QueueFamiliesIndices& indices)
	{
		uint32_t uQueueCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(device, &uQueueCount, nullptr);
		std::vector<VkQueueFamilyProperties> availibleQueues(uQueueCount);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &uQueueCount, availibleQueues.data());

		auto surface = GraphicsSurface::Get();	

		int i = 0;
		for (const auto& queueFamily : availibleQueues)
		{
			if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
			{
				indices.GraphicsFamily = i;
			}

			VkBool32 presentSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(device, i, *(VkSurfaceKHR*)surface->RawSurface(), &presentSupport);

			if (presentSupport)
			{
				indices.PresentFamily = i;
			}

			i++;
		}

	}

	bool VulkanGraphicsPhysicalDevice::CheckRequiredExtensionsSupport(_In_ VkPhysicalDevice device)
	{
		uint32_t extensionCount = 0;
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);
		std::vector<VkExtensionProperties> availibleExtensions(extensionCount);
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availibleExtensions.data());

		for (auto& e : _RequiredExtensions)
		{
			if (std::find(availibleExtensions.begin(), availibleExtensions.end(), e) == availibleExtensions.end())
				return false;
		}

		return true;
	}

	VulkanGraphicsPhysicalDevice::VulkanGraphicsPhysicalDevice()
	{
		auto gcontext = GraphicsContext::Get();
		VkInstance instance = static_cast<VkInstance>(gcontext->RawContext());

		uint32_t deviceCount = 0;
		vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);

		std::vector<VkPhysicalDevice> availibleDevices(deviceCount);
		vkEnumeratePhysicalDevices(instance, &deviceCount, availibleDevices.data());

		std::multimap<uint32_t, VkPhysicalDevice> rated;
		for (auto& d : availibleDevices)
		{
			rated.insert(
				std::make_pair(RateDeviceCapabilities(d), d));
		}
		
		RIFT_CORE_ASSERT(rated.rbegin()->first > 0, "Did not find any suitable GPU.");
		_PhysicalDevice = rated.rbegin()->second;
		vkGetPhysicalDeviceProperties(_PhysicalDevice, &_Properties);
		vkGetPhysicalDeviceFeatures(_PhysicalDevice, &_Features);

		RF_CORE_INFO("GPU selected: {0}", _Properties.deviceName);

		FindQueueFamilies(_PhysicalDevice, _QueuesSupported);

		GraphicsPhysicalDevice::_pSingletonInstance = this;
	}

	VulkanGraphicsPhysicalDevice::~VulkanGraphicsPhysicalDevice() noexcept
	{
		
	}

	void* VulkanGraphicsPhysicalDevice::GetRawDevice()
	{
		return _PhysicalDevice;
	}

	VulkanGraphicsLogicalDevice::VulkanGraphicsLogicalDevice()
	{
		auto physicalDevice = GraphicsPhysicalDevice::Get();
		auto queuesSupported = physicalDevice->QueuesSupported();
		VkPhysicalDevice hPhysicalDevice = (VkPhysicalDevice)physicalDevice->GetRawDevice();

		float queuePrioriteies = 1.0f;
		std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
		std::set<uint32_t> uniqueQueueFamilies = { queuesSupported.GraphicsFamily.value(), queuesSupported.PresentFamily.value() };

		float queuePriority = 1.0f;
		for (uint32_t queueFamily : uniqueQueueFamilies) {
			VkDeviceQueueCreateInfo queueCreateInfo{};
			queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queueCreateInfo.queueFamilyIndex = queueFamily;
			queueCreateInfo.queueCount = 1;
			queueCreateInfo.pQueuePriorities = &queuePriority;
			queueCreateInfos.push_back(queueCreateInfo);
		}

		VkPhysicalDeviceFeatures deviceFeatures{}; // TODO

		VkDeviceCreateInfo createInfo{
			.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
			.queueCreateInfoCount = (uint32_t)queueCreateInfos.size(),
			.pQueueCreateInfos = queueCreateInfos.data(),
			.enabledExtensionCount = _RequiredExtensions.size(),
			.ppEnabledExtensionNames = _RequiredExtensions.data(),
			.pEnabledFeatures = &deviceFeatures,
		};

		if (ValidationLayer::Enabled())
		{
			createInfo.ppEnabledLayerNames = ValidationLayer::EnabledLayers();
			createInfo.enabledLayerCount = ValidationLayer::EnabledLayersCount();
		}
		else
		{
			createInfo.enabledLayerCount = 0;
		}

		RIFT_CORE_ASSERT((vkCreateDevice(hPhysicalDevice, &createInfo, nullptr, &_LogicalDevice) == VK_SUCCESS), "Could not create virtual device.");

		vkGetDeviceQueue(_LogicalDevice, queuesSupported.GraphicsFamily.value(), 0, &_GraphicsQueue);
		vkGetDeviceQueue(_LogicalDevice, queuesSupported.PresentFamily.value(), 0, &_PresentQueue);
	}

	VulkanGraphicsLogicalDevice::~VulkanGraphicsLogicalDevice() noexcept
	{
		vkDestroyDevice(_LogicalDevice, nullptr);
	}

	void* VulkanGraphicsLogicalDevice::GetRawDevice()
	{
		return _LogicalDevice;
	}
}