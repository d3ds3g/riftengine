#include "Platform/Vulkan/VulkanValidationLayer.h"
#include "Rift/Core/Log.h"

bool operator==(const VkLayerProperties& layerProps, const char* pLayerName)
{
	return std::strcmp(layerProps.layerName, pLayerName) == 0;
}

namespace Rift
{
	VulkanValidationLayer::VulkanValidationLayer()
	{
		uint32_t uLayerCount = 0;
		vkEnumerateInstanceLayerProperties(&uLayerCount, nullptr);

		_AvailibleLayers.resize(uLayerCount);
		vkEnumerateInstanceLayerProperties(&uLayerCount, _AvailibleLayers.data());
		RF_CORE_INFO("Available validation layers:");
		for (auto& l : _AvailibleLayers)
			RF_CORE_INFO(l.layerName);
	}

	void VulkanValidationLayer::ImplAttach(const char* pLayerName)
	{
		RIFT_CORE_ASSERT((std::find(_AvailibleLayers.begin(), _AvailibleLayers.end(), pLayerName) != _AvailibleLayers.end()),
			"Validation layer not availible!");
		_EnabledLayers.push_back(pLayerName);
	}

}

