#pragma once

#include "Rift/Core/Application.h"
#include "Rift/Core/Layer.h"
#include "Rift/Core/Log.h"
#include "Rift/GFX/ValidationLayer.h"
#include "Rift/GFX/GraphicsDevice.h"

#include <vulkan/vulkan.h>
#include <vector>

using namespace Rift;

class TestingLayer : public Layer
{
private:
public:

	void OnAttach() override
	{
	}

	void OnEvent(Event& e)
	{
	}
};