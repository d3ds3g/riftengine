#include "VulkanTestLayer.h"

using namespace Rift;

int main()
{
	Rift::Scope<Application> app = CreateScope<Application>(WindowInfo
		{ .Title = "Sandbox app"}
	);
	app->PushLayer(CreateRef<TestingLayer>());
	app->Run();
}